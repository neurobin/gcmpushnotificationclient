package com.neurobin.androidpushnotification.pushNotification;

import android.content.Context;
import android.content.Intent;

public final class PushCommonUtilities {
	
	// put your server registration url here, must end with a /
    static final String SERVER_URL = "http://localhost/gcm/"; 

    // Google project id
    public static final String SENDER_ID = "xxxxxxxxxxxxx";
    public static String regId="";
    //Notification types
    public static String notificationType[]={"default","type1"};

    /**
     * Tag used on log messages.
     */
    static final String TAG = "androidpushnotification";

    public static final String DISPLAY_MESSAGE_ACTION =
            "com.neurobin.androidpushnotification.pushNotification.DISPLAY_MESSAGE";

    static final String EXTRA_MESSAGE = "message";

    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
}
