package com.neurobin.androidpushnotification.pushNotification;

import static com.neurobin.androidpushnotification.pushNotification.PushCommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.neurobin.androidpushnotification.pushNotification.PushCommonUtilities.SENDER_ID;

import com.google.android.gcm.GCMRegistrar;
import com.neurobin.androidpushnotification.pushNotification.PushCommonUtilities;
import com.neurobin.androidpushnotification.pushNotification.PushServerUtilities;
import com.neurobin.androidpushnotification.pushNotification.PushWakeLocker;
import com.neurobin.androidpushnotification.R;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class PushNotificationMainActivity extends Activity implements OnSharedPreferenceChangeListener {
	
	// label to display gcm messages

		SharedPreferences prefs;
		// Asyntask
		AsyncTask<Void, Void, Void> mRegisterTask;
		
		// Alert dialog manager
//		AlertDialogManager alert = new AlertDialogManager();
		
		// Connection detector

		
		//public static String name;
		//public static String email;

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_main);

	        prefs = PreferenceManager.getDefaultSharedPreferences(this);
			
//	        Log.i("fklajsdlkfjlas",""+prefs.getBoolean("first", true));
			
			
//			cd = new ConnectionDetector(getApplicationContext());
	//
//			// Check if Internet present
//			if (!cd.isConnectingToInternet()) {
//				// Internet Connection is not present
//				alert.showAlertDialog(PushNotificationMainActivity.this,
//						"Internet Connection Error",
//						"Please connect to working Internet connection", false);
//				// stop executing code by return
//				return;
//			}
//			
			// Getting name, email from intent
//			Intent i = getIntent();
//			
//			name = i.getStringExtra("name");
//			email = i.getStringExtra("email");		
			//name="Neurobin";
			//email="androidpushnotification@Neurobin";
			// Make sure the device has the proper dependencies.
			GCMRegistrar.checkDevice(this);

			// Make sure the manifest was properly set - comment out this line
			// while developing the app, then uncomment it when it's ready.
			GCMRegistrar.checkManifest(this);

			//lblMessage = (TextView) findViewById(R.id.lblMessage);
			
			registerReceiver(mHandleMessageReceiver, new IntentFilter(
					DISPLAY_MESSAGE_ACTION));
			
			// Get GCM registration id
			final String regId = GCMRegistrar.getRegistrationId(this);

			// Check if regid already presents
			if (regId.equals("")) {
				// Registration is not present, register now with GCM			
				GCMRegistrar.register(this, SENDER_ID);
				
			} 

			
			
			//if(!prefs.getBoolean("first", true)){GCMRegistrar.unregister(this);}
		
			
				//else {
				// Device is already registered on GCM
//				if (GCMRegistrar.isRegisteredOnServer(this)) {
//					// Skips registration.	
//					//lblMessage.setText("Already registered");
//					Toast.makeText(getApplicationContext(), "Already registered with GCM", Toast.LENGTH_LONG).show();
//				} else {
					// Try to register again, but not in the UI thread.
					// It's also necessary to cancel the thread onDestroy(),
					// hence the use of AsyncTask instead of a raw thread.
					final Context context = this;
					mRegisterTask = new AsyncTask<Void, Void, Void>() {

						@Override
						protected Void doInBackground(Void... params) {
							// Register on our server
							// On server creates a new user
							PushCommonUtilities.regId=regId;
							if(prefs.getBoolean("first", true)&&!regId.equals("")){PushServerUtilities.register(context, regId);}
							else{PushServerUtilities.unregister(context, regId);}
							return null;
						}

						@Override
						protected void onPostExecute(Void result) {
							mRegisterTask = null;
						}

					};
					mRegisterTask.execute(null, null, null);
				//}
			//}
					prefs.registerOnSharedPreferenceChangeListener(this);

						

			
					
						
					

					


					
					
		}		

		/**
		 * Receiving push messages
		 * */
		private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				//String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
				// Waking up mobile if it is sleeping
				PushWakeLocker.acquire(getApplicationContext());
				
				/**
				 * Take appropriate action on this message
				 * depending upon your app requirement
				 * For now i am just displaying it on the screen
				 * */
				
				// Showing received message
				//lblMessage.append(newMessage + "\n");			
				//Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();
				
				// Releasing wake lock
				PushWakeLocker.release();
			}
		};
		
		@Override
		protected void onDestroy() {
			if (mRegisterTask != null) {
				mRegisterTask.cancel(true);
			}
			try {
				unregisterReceiver(mHandleMessageReceiver);
				GCMRegistrar.onDestroy(this);
			} catch (Exception e) {
				//Log.e("UnRegister Receiver Error", "> " + e.getMessage());
			}
			super.onDestroy();
		}

		@Override
		public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
				String key) {
			// TODO Auto-generated method stub

			

			
			if(key.equals("first")){
				final String regId = GCMRegistrar.getRegistrationId(this);
				// Check if regid already presents
				if (regId.equals("")) {
					// Registration is not present, register now with GCM			
					GCMRegistrar.register(this, SENDER_ID);
					
				} 
		
				final Context context = this;
				mRegisterTask = new AsyncTask<Void, Void, Void>() {

					@Override
					protected Void doInBackground(Void... params) {
						// Register on our server
						// On server creates a new user
						//CommonUtilities.regId=regId;
						if(prefs.getBoolean("first", true)&&!regId.equals("")){PushServerUtilities.register(context, regId);}
						else {PushServerUtilities.unregister(context, regId);}
						
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						mRegisterTask = null;
					}

				};
				mRegisterTask.execute(null, null, null);
				
				if(prefs.getBoolean(key, true)){Toast.makeText(this, "Notification Enabled", Toast.LENGTH_SHORT).show();}
				else {Toast.makeText(this, "Notification Disabled", Toast.LENGTH_SHORT).show();}
			

			}
			
			if(key.equals("second")){
				if(prefs.getBoolean(key, true)){
					
					
					Toast.makeText(this, "Sound Enabled", Toast.LENGTH_SHORT).show();}
				
				else {
					
					
					Toast.makeText(this, "Sound Disabled", Toast.LENGTH_SHORT).show();}
			}
			
			if(key.equals("third")){
				if(prefs.getBoolean(key, true)){
					
					
					Toast.makeText(this, "Vibration Enabled", Toast.LENGTH_SHORT).show();}
				
				else {
					
					
					Toast.makeText(this, "Vibration Disabled", Toast.LENGTH_SHORT).show();}
				
			}
			
			
		}

	}



