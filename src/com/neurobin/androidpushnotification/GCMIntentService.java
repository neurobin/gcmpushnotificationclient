package com.neurobin.androidpushnotification;

import static com.neurobin.androidpushnotification.pushNotification.PushCommonUtilities.SENDER_ID;
import static com.neurobin.androidpushnotification.pushNotification.PushCommonUtilities.notificationType;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.neurobin.androidpushnotification.pushNotification.PushServerUtilities;
import com.neurobin.androidpushnotification.R;

public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCMIntentService";

	public GCMIntentService() {
		super(SENDER_ID);
	}

	/**
	 * Method called on device registered
	 **/
	@Override
	protected void onRegistered(Context context, String registrationId) {
		// Log.i(TAG, "Device registered: regId = " + registrationId);
		// Log.d("NAME", PushNotificationMainActivity.name);
		PushServerUtilities.register(context, registrationId);
	}

	/**
	 * Method called on device un registred
	 * */
	@Override
	protected void onUnregistered(Context context, String registrationId) {
		// Log.i(TAG, "Device unregistered");
		PushServerUtilities.unregister(context, registrationId);
	}

	/**
	 * Method called on Receiving a new message
	 * */
	@Override
	protected void onMessage(Context context, Intent intent) {
		// Log.i(TAG, "Received message");
		String message = "", type = "";

		for (int i = 0; i < notificationType.length; i++) {
			if (!intent.getExtras().getString(notificationType[i]).isEmpty()) {
				message = intent.getExtras().getString(notificationType[i]);
				type = notificationType[i];
			}

		}

		// notifies user
		if (!message.equals(""))
			generateNotification(context, message, type);
	}

	/**
	 * Method called on receiving a deleted message
	 * */
	@Override
	protected void onDeletedMessages(Context context, int total) {
		// Log.i(TAG, "Received deleted messages notification");
		String message = getString(R.string.gcm_deleted, total);
		// notifies user
		generateNotification(context, message, "default");
	}

	/**
	 * Method called on Error
	 * */
	@Override
	public void onError(Context context, String errorId) {
		Log.i(TAG, "Received error: " + errorId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		Log.i(TAG, "Received recoverable error: " + errorId);
		// errorId));
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private static void generateNotification(Context context, String message,
			String type) {
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);

		String title = context.getString(R.string.app_name);
		Intent notificationIntent = new Intent(context, MainActivity.class);

		if (type.equalsIgnoreCase("type1")) {
			notificationIntent = new Intent(context, MainActivity.class);
		}
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Play default notification sound
		if (prefs.getBoolean("second", true))
			notification.defaults |= Notification.DEFAULT_SOUND;

		// notification.sound = Uri.parse("android.resource://" +
		// context.getPackageName() + "your_sound_file_name.mp3");

		// Vibrate if vibrate is enabled
		if (prefs.getBoolean("third", true))
			notification.defaults |= Notification.DEFAULT_VIBRATE;

		notificationManager.notify(0, notification);

	}

}
